#!/usr/bin/env Rscript

rm(list = ls())

library(Rcpp)

cat("Sourcing R script...", "\n")
print(system.time(source("func_r.R")))
cat("\n")
cat("Compiling and sourcing C++ source file...", "\n")
print(system.time(Rcpp::sourceCpp("func_cpp.cpp")))
cat("\n")

N_TESTS = 3L
N_CALLS = 3000L
SZ_MATS = 100L
lockBinding("N_TESTS", globalenv())
lockBinding("N_CALLS", globalenv())
lockBinding("SZ_MATS", globalenv())

cat("Number of test(s) per case:", N_TESTS, "\n")
cat("Number of function call(s):", N_CALLS, "\n")
cat("Size of matrices in function:", SZ_MATS, "by", SZ_MATS, "\n")
cat("\n")

cat("Testing vanilla R function...", "\n")
for (knd in 1L:N_TESTS) {
    x = matrix(runif(SZ_MATS * SZ_MATS), nrow=SZ_MATS, ncol=SZ_MATS)
    cat(sprintf("Loop %d of %d:", knd, N_TESTS), "\n")
    print(system.time(for (jnd in 1L:N_CALLS) {multiply_r(x, x)}))
}
cat("\n")

cat("Testing Rcpp function...", "\n")
for (knd in 1L:N_TESTS) {
    x = matrix(runif(SZ_MATS * SZ_MATS), nrow=SZ_MATS, ncol=SZ_MATS)
    cat(sprintf("Loop %d of %d:", knd, N_TESTS), "\n")
    print(system.time(for (jnd in 1L:N_CALLS) {multiply_cpp(x, x)}))
}
cat("\n")
