/***
 * func_cpp.cpp
 *
 * Created: 2018-2-6
 *  Author: Charles Zhu
 */

// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>
using namespace Rcpp;

// [[Rcpp::export]]
arma::Mat<double> multiply_cpp(
        const arma::Mat<double>& x,
        const arma::Mat<double>& y
    ) {
    if (x.n_rows <= 0 || x.n_cols <= 0) {
        throw(std::length_error("x.n_rows <= 0 || x.n_cols <= 0"));
    }
    if (y.n_rows <= 0 || y.n_cols <= 0) {
        throw(std::length_error("y.n_rows <= 0 || y.n_cols <= 0"));
    }
    if (x.n_cols != y.n_rows) {
        throw(std::length_error("x.n_cols != y.n_rows"));
    }

    return x * y;
}
